const request = require('supertest');

const app = require('../app');

test("For the store analytics", async () => {
	await request(app).post('/analytics')
	.send({
		ip: "192.168.43.1",
		coordinates : {
			x : 50,
			y : 70
		}
	})
	.expect(200)
})


test("For the get distance analytics", async () => {
	await request(app).get('/analytics?ip=192.168.43.1')
	.send()
	.expect(200)
})