const IPActivity = require('../models/ipactivity')
const catchAsync = require('../services/core/CatchAsync')
const {RequestError} = require('../services/Exceptions')
const ipServices = require('../services/IpServices')


exports.store = catchAsync(async(req, res, next) => {
	let { ip , coordinates } = req.body
	let ipdata = await IPActivity.findOne(ip)
	if (!ipdata) ipdata = await IPActivity.create({ip, coordinates : []})
	await ipServices.UpdateCoordinate(ipdata, coordinates)
	res.status(200).json({msg : 'ok'})
})


exports.get = catchAsync(async(req,res, next) => {
	let { ip } = req.query
	let ipdata = await IPActivity.findOne(ip)
	if (!ipdata) throw new RequestError('We dont have this IP ADDRESS')
	let result = ipServices.getDistance(ipdata.data.coordinates)
	res.status(200).json({result})
})

exports.getAll = catchAsync(async(req, res, next)=> {
	let all = await IPActivity.find()
	res.status(200).json(all)
})