
const IPActivityController = require('../controllers/IPActivityController')
const Validate = require ("../middlewares/Validate")
const Request = require ("../middlewares/Request")
const route = require('express').Router()
const path = require('path')

route.get('/all', IPActivityController.getAll)
route.get('/analytics', Validate.ip, IPActivityController.get)
route.post('/analytics', Validate.ip, Request.check, IPActivityController.store)
route.get('/docs', (req, res)=>res.sendFile(path.join(__dirname, '../docs/ipactivityDocs.json')))

module.exports = route