// Imports

const compression = require('compression')
const debug = require('debug')('http')
const express = require("express")
const path = require("path")

const rateLimit = require('express-rate-limit')

// Init App
const app = express()


// Error Handler
const { errorHandler } = require("./services/core/Exception")

const limiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 minutes
  max: 100, // limit each IP to 100 requests per windowMs
  message: 'You have exceeded the 100 requests in 24 hrs limit!', 
  headers: true,
})


// Allow Json Parser
app.use(express.urlencoded({extended: false}))
app.use(express.json())

//  Limits the amount of time required for a request to be processed
app.use(limiter);

// Headers SetUp
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST'
    );
    next();
});

// Dev Info
if (process.env.NODE_ENV == 'development'){
	app.use((req, res, next) => {
		console.log(`::VISITING: ${req.url}`)
        console.log(`::QUERY: ${JSON.stringify(req.body || req.query || req.params)}`)
        console.log(`---------------------------------------------`)
		next()
	})
}

if (process.env.NODE_ENV=='production') {
    app.use(compression())
    app.use((req, res, next)=>{
        debug(req.method + ' ' + req.url)
        next()
    })
}

// Routes
app.use('/', require('./routes/home'))
app.get('*', (req, res)=>{
  res.status(404).send({
    msg : "Route not found"
  })
})

// Error Handler route
app.use(errorHandler)

module.exports = app