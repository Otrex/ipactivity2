// Env File SetUp
require("dotenv").config({path : "./.env"})

// Connect App to DB
// require("./database").connect()
// const db = require('./models')

// App SetUp
const app = require('./app')

// Port SetUp
PORT = process.env.PORT || 3000

// Serve App
app.listen(PORT, ()=>{
	console.log("=======================================================")
	console.log(`${process.env.NODE_ENV} SERVER STARTED :: PORT:${PORT}`)
	console.log("=======================================================")
})