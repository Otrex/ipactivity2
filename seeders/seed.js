const { random } = require('../utils/tools')
const IPActivity = require('../models/ipactivity.js')

const generateCoordinates = (n) => {
  let coords = []
    for (var i = 0; i <= n; i++ ){
      coords.push({
        x: random(3), y:random(3), 
        createdAt : new Date(new Date() - (random(undefined, 0,20)/10) * 60 * 60 * 1000)
      })
    }
  return coords
}

const generateIps = (n)=>{
	// First ip for testing
  	let ips = [{ip : "192.168.43.1", coordinates : generateCoordinates(5)}]
  	for (var i = 0; i <= n; i++ ){
	    ips.push({
	        ip: `${random(3)}.${random(3)}.${random(2)}.${random(1)}`,
	        coordinates : generateCoordinates(5)
	    })
  	}
  return ips
}

IPActivity.write(generateIps(5));

console.log('Seeding complete....')
