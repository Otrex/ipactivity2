var events = require('events');
var eventEmitter = new events.EventEmitter();

//Create an event handler to delete old records
var deleteEvent = async function () {
  await coordinate.where({createdAt : {$lte : new Date(new Date() - 1 * 60 * 60 * 1000).getTime()}}).delete()
}

//Assign the event handler to an event:
eventEmitter.on('clear-expired-coordinates', deleteEvent);


module.exports = eventEmitter