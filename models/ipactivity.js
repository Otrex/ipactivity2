const fs  = require('fs')

class IPActivity {
  constructor() {
    this.data = {}
  }

  async find () {
    return await this.read();
  }

  async read () {
    return JSON.parse(await fs.readFileSync('./models/ipactivity.json'))
  }

  async write (data) {
    await fs.unlinkSync('./models/ipactivity.json')
    return await fs.writeFileSync('./models/ipactivity.json', JSON.stringify(data));
  }

 async create (data) {
   if (data) this.data = data
    return this
 }

 async findOne (ip) {
   let file = await this.read()
   this.data = file.find(rec => rec.ip === ip)
   return this.data ? this : null
 }

 async save() {
   let file = await this.read()
   file = file.filter(rec => rec.ip != this.data.ip)
   file.push (this.data)
   await this.write (file)
   return this
 }
}


module.exports = new IPActivity() 