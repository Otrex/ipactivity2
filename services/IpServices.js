const { DBError } = require ('./Exceptions')
const event = require('../events/cleaner')
const fnc = require('../utils/tools')

// Update the coordinates for a particular ip
exports.UpdateCoordinate = (ipmodel, coordinate) => {
	try {
		coordinate.createdAt = new Date().getTime()
		ipmodel.data.coordinates.push(coordinate)
		ipmodel.save()
	} catch (err) {
		throw new DBError (err)
	}
}


// Calculates the distance
exports.getDistance = (coordinates) => {
	// To ensure that the list of coordinates for distance calculation has been created
	// not more than One hour prior to the now
	coordinates = coordinates.filter(coord => fnc.LTEoneHour(coord.createdAt))
	let firstCoordinate = coordinates[0]
	let distance = 0
	coordinates.forEach(coord => {
		// At the first iteration, the distance between the firstCoordinate and coord would be Zero
		distance += fnc.distanceBetweenTwoPoint(coord, firstCoordinate)
		firstCoordinate = coord
	})

	// This removes all coordinates that has exceeded one hour
	// event.emit('clear-expired-coordinates');
	return {distance}
}