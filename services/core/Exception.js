const logger = require('debug')('apperror')

class AppError extends Error {
	constructor (name, message, statusCode, logit=false){
		super()
		this.name = name || "GeneralError"
		this.message = message
		this.statusCode = statusCode
		this.logit = logit
		if (this.logit) this.log()
	}

	log () {
		logger(`${this.name.toUpperCase()}::${this.message}::STATUS-CODE:${this.statusCode}`)
	}
}



const publishError = (err, res) => {
	if (err.name === 'ValidationError') {
		res.status(400).json ({
			name : err.name,
			message : err.message,
			errors : err.errors,
	        status : "failed"
	    })
	    return
	}

	res.status(err.statusCode || 400).json ({
		name : err.name,
        status: err.status || 'failed',
        message: err.message,
        // stack: err.stack
    })
}

const errorHandler = (err, req, res, next) => {
	if (process.env.NODE_ENV === 'development') {
		publishError(err, res)
		next()
	}
	if (process.env.NODE_ENV === 'production') {
		publishError(err, res)
	}
}


module.exports = {
	AppError,
	errorHandler
}